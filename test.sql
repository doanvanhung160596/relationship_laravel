/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : test

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 21/09/2019 16:42:48
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'category name',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES (1, 'category name 1', '2019-09-03 14:52:06', '2019-09-03 14:52:06');
INSERT INTO `category` VALUES (2, 'category name 2', '2019-09-03 14:52:06', '2019-09-03 14:52:06');
INSERT INTO `category` VALUES (3, 'category name 3', '2019-09-03 14:52:06', '2019-09-03 14:52:06');
INSERT INTO `category` VALUES (4, 'category name 4', '2019-09-03 14:52:06', '2019-09-03 14:52:06');
INSERT INTO `category` VALUES (5, 'category name 5', '2019-09-03 14:52:06', '2019-09-03 14:52:06');

-- ----------------------------
-- Table structure for comments
-- ----------------------------
DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'title comment',
  `commentable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `commentable_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `comments_commentable_type_commentable_id_index`(`commentable_type`, `commentable_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of comments
-- ----------------------------
INSERT INTO `comments` VALUES (1, 'A new comment.', 'App\\Post', 1, '2019-09-21 03:01:40', '2019-09-21 03:01:40');
INSERT INTO `comments` VALUES (2, 'A new comment.', 'App\\Post', 2, '2019-09-21 03:02:17', '2019-09-21 03:02:17');
INSERT INTO `comments` VALUES (3, 'A new comment.', 'App\\Post', 1, '2019-09-21 03:02:27', '2019-09-21 03:02:27');
INSERT INTO `comments` VALUES (4, 'A new comment.', 'App\\Post', 3, '2019-09-21 03:03:19', '2019-09-21 03:03:19');
INSERT INTO `comments` VALUES (5, 'A new comment.', 'App\\Post', 3, '2019-09-21 03:03:30', '2019-09-21 03:03:30');
INSERT INTO `comments` VALUES (6, 'A new comment.', 'App\\Question', 3, '2019-09-21 03:04:59', '2019-09-21 03:04:59');
INSERT INTO `comments` VALUES (7, 'A new comment.', 'App\\Question', 4, '2019-09-21 03:05:05', '2019-09-21 03:05:05');
INSERT INTO `comments` VALUES (8, 'A new comment.', 'App\\Question', 4, '2019-09-21 03:05:06', '2019-09-21 03:05:06');
INSERT INTO `comments` VALUES (9, 'A new comment.', 'App\\Question', 4, '2019-09-21 03:05:07', '2019-09-21 03:05:07');
INSERT INTO `comments` VALUES (10, 'A new comment.', 'App\\Question', 1, '2019-09-21 03:05:11', '2019-09-21 03:05:11');
INSERT INTO `comments` VALUES (11, 'A new comment.', 'App\\Question', 1, '2019-09-21 03:05:12', '2019-09-21 03:05:12');
INSERT INTO `comments` VALUES (12, 'A new comment.', 'App\\Question', 2, '2019-09-21 03:05:16', '2019-09-21 03:05:16');
INSERT INTO `comments` VALUES (13, 'A new comment.', 'App\\Question', 5, '2019-09-21 03:05:21', '2019-09-21 03:05:21');

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for images
-- ----------------------------
DROP TABLE IF EXISTS `images`;
CREATE TABLE `images`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'title image',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of images
-- ----------------------------
INSERT INTO `images` VALUES (1, 'title image 1', '2019-09-03 14:52:06', '2019-09-03 14:52:06');
INSERT INTO `images` VALUES (2, 'title image 2', '2019-09-03 14:52:06', '2019-09-03 14:52:06');
INSERT INTO `images` VALUES (3, 'title image 3', '2019-09-03 14:52:06', '2019-09-03 14:52:06');
INSERT INTO `images` VALUES (4, 'title image 4', '2019-09-03 14:52:06', '2019-09-03 14:52:06');
INSERT INTO `images` VALUES (5, 'title image 5', '2019-09-03 14:52:06', '2019-09-03 14:52:06');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO `migrations` VALUES (4, '2019_09_20_051034_create_product_table', 1);
INSERT INTO `migrations` VALUES (5, '2019_09_20_051110_create_category_table', 1);
INSERT INTO `migrations` VALUES (6, '2019_09_20_051132_create_product_categories_table', 1);
INSERT INTO `migrations` VALUES (10, '2019_09_21_021816_create_comments_table', 2);
INSERT INTO `migrations` VALUES (11, '2019_09_21_022004_create_posts_table', 2);
INSERT INTO `migrations` VALUES (12, '2019_09_21_022019_create_questions_table', 2);
INSERT INTO `migrations` VALUES (13, '2019_09_21_073812_create_videos_table', 3);
INSERT INTO `migrations` VALUES (14, '2019_09_21_074116_create_images_table', 3);
INSERT INTO `migrations` VALUES (15, '2019_09_21_074132_create_tags_table', 3);
INSERT INTO `migrations` VALUES (16, '2019_09_21_074215_create_taggables_table', 3);
INSERT INTO `migrations` VALUES (17, '2019_09_21_092105_edit_column_title_video_table', 4);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for posts
-- ----------------------------
DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'title post',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of posts
-- ----------------------------
INSERT INTO `posts` VALUES (1, 'title post 1', '2019-09-03 14:52:06', '2019-09-03 14:52:06');
INSERT INTO `posts` VALUES (2, 'title post 2', '2019-09-03 14:52:06', '2019-09-03 14:52:06');
INSERT INTO `posts` VALUES (3, 'title post 3', '2019-09-03 14:52:06', '2019-09-03 14:52:06');

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'product name',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES (1, 'product name 1', '2019-09-03 14:52:06', '2019-09-03 14:52:06');
INSERT INTO `product` VALUES (2, 'product name 2', '2019-09-03 14:52:06', '2019-09-03 14:52:06');
INSERT INTO `product` VALUES (3, 'product name 3', '2019-09-03 14:52:06', '2019-09-03 14:52:06');
INSERT INTO `product` VALUES (4, 'product name 4', '2019-09-03 14:52:06', '2019-09-03 14:52:06');
INSERT INTO `product` VALUES (5, 'product name 5', '2019-09-03 14:52:06', '2019-09-03 14:52:06');

-- ----------------------------
-- Table structure for product_categories
-- ----------------------------
DROP TABLE IF EXISTS `product_categories`;
CREATE TABLE `product_categories`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_categories
-- ----------------------------
INSERT INTO `product_categories` VALUES (4, 1, 4);
INSERT INTO `product_categories` VALUES (5, 1, 5);
INSERT INTO `product_categories` VALUES (6, 2, 1);
INSERT INTO `product_categories` VALUES (7, 2, 2);
INSERT INTO `product_categories` VALUES (8, 2, 3);
INSERT INTO `product_categories` VALUES (9, 2, 4);
INSERT INTO `product_categories` VALUES (10, 2, 5);
INSERT INTO `product_categories` VALUES (11, 3, 1);
INSERT INTO `product_categories` VALUES (12, 3, 2);
INSERT INTO `product_categories` VALUES (13, 3, 3);
INSERT INTO `product_categories` VALUES (14, 3, 4);
INSERT INTO `product_categories` VALUES (15, 3, 5);
INSERT INTO `product_categories` VALUES (16, 4, 1);
INSERT INTO `product_categories` VALUES (17, 4, 2);
INSERT INTO `product_categories` VALUES (18, 4, 3);
INSERT INTO `product_categories` VALUES (19, 4, 4);
INSERT INTO `product_categories` VALUES (26, 4, 5);
INSERT INTO `product_categories` VALUES (27, 5, 1);
INSERT INTO `product_categories` VALUES (28, 5, 2);
INSERT INTO `product_categories` VALUES (29, 5, 3);
INSERT INTO `product_categories` VALUES (30, 5, 4);

-- ----------------------------
-- Table structure for questions
-- ----------------------------
DROP TABLE IF EXISTS `questions`;
CREATE TABLE `questions`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'title question',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of questions
-- ----------------------------
INSERT INTO `questions` VALUES (1, 'title question 1', '2019-09-03 14:52:06', '2019-09-03 14:52:06');
INSERT INTO `questions` VALUES (2, 'title question 2', '2019-09-03 14:52:06', '2019-09-03 14:52:06');
INSERT INTO `questions` VALUES (3, 'title question 3', '2019-09-03 14:52:06', '2019-09-03 14:52:06');
INSERT INTO `questions` VALUES (4, 'title question 4', '2019-09-03 14:52:06', '2019-09-03 14:52:06');
INSERT INTO `questions` VALUES (5, 'title question 5', '2019-09-03 14:52:06', '2019-09-03 14:52:06');

-- ----------------------------
-- Table structure for taggables
-- ----------------------------
DROP TABLE IF EXISTS `taggables`;
CREATE TABLE `taggables`  (
  `tag_id` int(11) NOT NULL,
  `taggable_id` int(11) NOT NULL,
  `taggable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of taggables
-- ----------------------------
INSERT INTO `taggables` VALUES (1, 1, 'App\\Video');
INSERT INTO `taggables` VALUES (2, 1, 'App\\Video');
INSERT INTO `taggables` VALUES (3, 1, 'App\\Video');
INSERT INTO `taggables` VALUES (3, 2, 'App\\Video');
INSERT INTO `taggables` VALUES (3, 3, 'App\\Video');
INSERT INTO `taggables` VALUES (3, 3, 'App\\Image');
INSERT INTO `taggables` VALUES (3, 2, 'App\\Image');
INSERT INTO `taggables` VALUES (3, 1, 'App\\Image');
INSERT INTO `taggables` VALUES (2, 1, 'App\\Image');
INSERT INTO `taggables` VALUES (2, 2, 'App\\Image');
INSERT INTO `taggables` VALUES (2, 3, 'App\\Image');
INSERT INTO `taggables` VALUES (1, 3, 'App\\Image');
INSERT INTO `taggables` VALUES (1, 2, 'App\\Image');
INSERT INTO `taggables` VALUES (1, 1, 'App\\Image');
INSERT INTO `taggables` VALUES (1, 1, 'App\\Image');

-- ----------------------------
-- Table structure for tags
-- ----------------------------
DROP TABLE IF EXISTS `tags`;
CREATE TABLE `tags`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'title tag',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tags
-- ----------------------------
INSERT INTO `tags` VALUES (1, 'title tag 1', '2019-09-03 14:52:06', '2019-09-03 14:52:06');
INSERT INTO `tags` VALUES (2, 'title tag 2', '2019-09-03 14:52:06', '2019-09-03 14:52:06');
INSERT INTO `tags` VALUES (3, 'title tag 3', '2019-09-03 14:52:06', '2019-09-03 14:52:06');
INSERT INTO `tags` VALUES (4, 'title tag 4', '2019-09-03 14:52:06', '2019-09-03 14:52:06');
INSERT INTO `tags` VALUES (5, 'title tag 5', '2019-09-03 14:52:06', '2019-09-03 14:52:06');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for videos
-- ----------------------------
DROP TABLE IF EXISTS `videos`;
CREATE TABLE `videos`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'title video',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of videos
-- ----------------------------
INSERT INTO `videos` VALUES (1, '2019-09-03 14:52:06', '2019-09-03 14:52:06', 'title video 1');
INSERT INTO `videos` VALUES (2, '2019-09-03 14:52:06', '2019-09-03 14:52:06', 'title video 2');
INSERT INTO `videos` VALUES (3, '2019-09-03 14:52:06', '2019-09-03 14:52:06', 'title video 3');
INSERT INTO `videos` VALUES (4, '2019-09-03 14:52:06', '2019-09-03 14:52:06', 'title video 4');
INSERT INTO `videos` VALUES (5, '2019-09-03 14:52:06', '2019-09-03 14:52:06', 'title video 5');

SET FOREIGN_KEY_CHECKS = 1;
