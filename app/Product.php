<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //    Many To Many
    protected $table = 'product';
    protected $guarded = ['id'];

    public function categories(){
        return $this->belongsToMany(Category::class, 'product_categories', 'product_id', 'category_id', 'id');
    }
}
