<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //  One To Many (Polymorphic)
    protected $guarded = ['id'];

    public function commentable(){
        return $this->morphTo();
    }
}
