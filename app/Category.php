<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
//    Many To Many
    protected $table = 'category';
    protected $guarded = ['id'];

    public function product(){
        return $this->belongsToMany(Product::class, 'product_categories', 'category_id', 'product_id', 'id');
    }
}
