<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    //  One To Many (Polymorphic)
    protected $guarded = ['id'];

    public function comments(){
        return $this->morphMany(Comment::class, 'commentable');
    }
}
