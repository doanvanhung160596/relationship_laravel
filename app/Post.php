<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //  One To Many (Polymorphic)
    public function comments(){
        return $this->morphMany(Comment::class, 'commentable');
    }
}
