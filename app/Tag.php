<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $guarded = ['id'];

    /**
     * Get all of the posts that are assigned this tag.
     */
    public function videos()
    {
        return $this->morphedByMany(Video::class, 'taggable');
    }

    /**
     * Get all of the videos that are assigned this tag.
     */
    public function images()
    {
        return $this->morphedByMany(Image::class, 'taggable');
    }
}
